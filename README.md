###项目结构说明
```
┌── .editorconfig                                     # 编译配置
├── README.md                                         # 项目说明
├── babel.config.js                                   # babel配置
├── package.json                                      # 依赖包配置
├── tsconfig.json                                     # ts配置
├── vue.config.js                                     # vue项目运行配置
├── src                                               # 编译目录
│   ├── main.ts                                       # 入口文件
│   ├── views                                         # 视图目录(不做路由的)
│   ├── types                                         # ts类型定义
│   │   ├── shime-axios.d.ts                          # axios类型定义
│   │   ├── shime-global.d.ts                         # 全局类型定义
│   │   ├── shime-vue-router                          # vue-router类型定义
│   │   ├── shims-tsx.d.ts                            # jsx类型定义
│   │   └── shims-vue.d.ts                            # vue类型定义
│   ├── store                                         # vuex模块目录(自动导入vuex)
│   ├── static                                        # 静态目录(存放js,html,json等)
│   ├── plugins                                       # 插件目录
│   │   ├── axios                                     # axios插件 
│   │   ├── vuex                                      # vuex插件 
│   │   ├── vue-router                                # vue-router 插件
│   │   ├── vue                                       # vue 插件(全局引入Vue的插件定义)
│   │   └── observer                                  # vue 全局观察者定义
│   ├── pages                                         # 页面目录(自动根据目录导入路由)
│   ├── layouts                                       # 布局目录
│   ├── libs                                          # js库文件目录
│   ├── errors                                        # 错误页面目录
│   ├── config                                        # 配置定义目录
│   │   ├─ common.ts                                  # 通用环境配置
│   │   ├─ dev.ts                                     # 开发环境配置
│   │   ├─ global.ts                                  # 全局配置定义
│   │   └─ pro.ts                                     # 生成环境配置
│   ├── components                                    # 组件目录
│   └── assets                                        # 静态资源目录(存放img,svg等)
└── public                                            # 静态资源访问目录
    ├── favicon.ico                                   # 页面图标
    └── index.html                                    # 首页
```
