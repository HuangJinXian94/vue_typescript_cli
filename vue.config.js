//常量
const BASE_TITLE = "TypeScript版手脚架";
const BASE_URL = (process.env.NODE_ENV === 'production') ? '/' : '/';

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
   publicPath: BASE_URL,
   configureWebpack: config => {
      const env = process.env.NODE_ENV;
      switch (env) {
         case 'development':
            console.log('----->当前为development环境\n');
            break;
         case 'production':
            console.log('----->当前为production环境\n');
            break;
         default:
            console.error('----->当前为环境未配置\n');
            break;
      }
      // 配置插件
      let plugins = [
         new HtmlWebpackPlugin({
            template: './src/static/index.html',
            title: BASE_TITLE,
            baseUrl: BASE_URL,
            hash: true,
            minify: {
               // 去除双引号
               removeAttributeQuotes: (process.env.NODE_ENV === 'production'),
               // 合并代码到一行
               collapseWhitespace: (process.env.NODE_ENV === 'production'),
            }
         })
      ];
      config.plugins = [...config.plugins, ...plugins];
   },
   chainWebpack: (config) => {
      // 配置别名
      config.extensions = ['.js', '.ts', '.vue'];
      config.resolve.alias.set('@', path.resolve(__dirname, 'src'));
      // 启用代码分析
      if (process.env.npm_config_report) {
         config.plugin('webpack-bundle-analyzer').use(
            require('webpack-bundle-analyzer').BundleAnalyzerPlugin
         );
      }
   },
   // 配置开发服务器
   devServer: {
      contentBase: path.join(__dirname, 'dist'),
      port: 8000,
      compress: true, // 自动压缩
      open: true, // 自动打开浏览器
      disableHostCheck: true,
   }
};
