//框架
import Vue from 'vue'
import '@/plugins/vue'
import router from '@/plugins/vue-router'
import store from '@/plugins/vuex'
import data from '@/plugins/observer'

//导入组件
import App from './layouts/App.vue'

//组装vue实例
new Vue({
   router,
   store,
   data,
   render: h => h(App)
}).$mount('#app');
