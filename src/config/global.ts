/**
 * 全局定义
 */

//抽象定义
export interface Api {
    prefix: string
    platform: string
    login: string
    isDev: boolean
}

interface Global {
    indexPagePath: string
    errorComponentName: string
    pageBaseTitle: string
    pageDefaultRequiresAuth: boolean
    pageDefaultKeepAlive: boolean
    cacheKeyJwt: string
    pageBaseUrl: string
    PLATFORM_URL: string
    BBI_URL: string
    API: Api,
    desKey:string,
    desIv:string,
    desName:string,
}

//导入通用配置
const common = Object.assign({}, require("./common"));

//导入对应环境配置
const global: Global = Object.assign({}, common, require(
    (process.env.NODE_ENV === 'production') ?
        "@/config/pro"
        :
        "@/config/dev"
    )
);

export default global

