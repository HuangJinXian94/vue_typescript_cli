/**
 * 生成环境配置
 */

import {Api} from './global'

//页面公用的地址
export const pageBaseUrl: string = '/';

//平台地址
export const PLATFORM_URL: string = '';

//系统地址
export const BBI_URL: string = '';

//API相关信息
export const API: Api = {
    prefix: BBI_URL + '/backend',
    platform: PLATFORM_URL,
    login: PLATFORM_URL + '/#/login',
    isDev: true
};
