/**
 * 通用环境配置
 */

//首页页面路径
export const indexPagePath: string = '/index';

//错误组件名
export const errorComponentName: string = 'error404';

//页面公用的标题
export const pageBaseTitle: string = 'datamaker';

//页面默认权限是否验证
export const pageDefaultRequiresAuth: boolean = false;

//页面默认组件是否激活
export const pageDefaultKeepAlive: boolean = false;

//缓存验证key
export const cacheKeyJwt: string = 'X-Auth-Token';

//加密的key
export const desKey = 'desKey';
//加密的签名
export const desIv = 'desIv';
//加密的名
export const desName = 'desName';
