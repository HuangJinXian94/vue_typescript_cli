import {
   Component,
   Vue,
   Mixins,
   Inject,
   InjectReactive,
   Provide,
   ProvideReactive,
   Model,
   Prop,
   PropSync,
   Watch,
   Emit,
   Ref,
} from 'vue-property-decorator';

import {
   State,
   Getter,
   Action,
   Mutation,
   namespace
} from 'vuex-class';

//解决Route过深,找不到类型
import {Route} from 'vue-router';
interface VueRoute extends Route{
}


import BaseVue from './BaseVue'


export {
   Component,
   Vue,
   Mixins,
   Inject,
   InjectReactive,
   Provide,
   ProvideReactive,
   Model,
   Prop,
   PropSync,
   Watch,
   Emit,
   Ref,

   State,
   Getter,
   Action,
   Mutation,
   namespace,

   VueRoute,

   BaseVue,
};
