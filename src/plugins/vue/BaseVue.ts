import {Vue,VueRoute} from './decorator'

//是否开发环境
const isDev = (process.env.NODE_ENV != 'production');

export default class BaseVue extends Vue {
   /**
    * 路由生命周期
    */
   static beforeRouteEnter(to: VueRoute, from: VueRoute, next: () => void): void {
      if(isDev){
         console.log("进入路由:"+from.path+" --> "+to.path);
      }
      next();
   }

   static beforeRouteLeave(to: VueRoute, from: VueRoute, next: () => void): void {
      if(isDev){
         console.log("离开路由:"+from.path+" --> "+to.path);
      }
      next();
   }

   static beforeRouteUpdate(to: VueRoute, from: VueRoute, next: () => void): void {
      if(isDev){
         console.log("更新路由:"+from.path+" --> "+to.path);
      }
      next();
   }

   /**
    * 组件生命周期
    */
   created(): void {
      if(isDev) {
         console.log("实例创建")
      }
   }

   mounted(): void {
      if(isDev) {
         console.log("实例渲染")
      }
   }

   beforeUpdate(): void {
      if(isDev) {
         console.log("实例更新前")
      }
   }

   beforeDestroy(): void {
      if(isDev) {
         console.log("实例注销前")
      }
   }
}

