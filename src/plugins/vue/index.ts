import Vue from 'vue'
import Component from 'vue-class-component'

//注册路由生命周期
Component.registerHooks([
    //进入路由前操作
    'beforeRouteEnter',
    //离开路由前操作
    'beforeRouteLeave',
    //路由更新操作
    'beforeRouteUpdate',
]);

//关闭警告提示
Vue.config.productionTip = false;

//导入样式
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element, {
   size: 'small',
   zIndex: 3000 ,
});

//导入Axios实现
import Axios from '@/plugins/axios'
Vue.prototype.$http=Axios;

export default Vue
