import Vue from 'vue'

const Observer = {
   loadEvent: new Vue(),
   sendEvent: new Vue(),
   broadEvent: new Vue(),
};

export default Observer
